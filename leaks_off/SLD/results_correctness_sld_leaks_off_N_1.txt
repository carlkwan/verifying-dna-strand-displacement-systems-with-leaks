filter(count, "deadlock"):
Result
1

filter(count, "complete"):
Result
1

E [ F "complete" ]:
Result
true

E [ F !"complete" ]:
Result
true

E [ F "deadlock"&!"complete" ]:
Result
false

A [ G "deadlock"=>"complete" ]:
Result
true

A [ F "complete" ]:
Result
true

P=? [ F output=N&gates_reactive>0 ]:
Result
0.0

P=? [ F output=N&strands_reactive>N ]:
Result
0.0

P=? [ F "complete" ]:
Result
1.0
