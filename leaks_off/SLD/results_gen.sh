#!/bin/bash
for i in 1 5 10 15 20 
do
	echo "---- N=$i start ----"
	./prism sld_leaks_off_N_"$i".sm correctness.csl -exportresults results_correctness_sld_leaks_off_N_"$i".txt
	./prism sld_leaks_off_N_"$i".sm performance.csl -const t=0:1000:70000 -exportresults results_performance_sld_leaks_off_N_"$i".txt
	./prism sld_leaks_off_N_"$i".sm performance_csv.csl -const t=0:1000:70000 -exportresults results_performance_sld_leaks_off_N_"$i".csv:csv
	echo "---- N=$i finish ----"
done
