# About
We verify a set of DNA strand displacement (DSD) protocols for single long domain 
(SLD), double long domain (DLD), and triple long domain (TLD) motifs. Prior attempts
to do the same fails to address leaks in the chemical reactions due to an explosion 
in the state space. This project takes into account such leaks. 

The reactions were simulated with Visual DSD, the protocols were verified with the 
probabilistic model checker PRISM, and statistical analyses and figures were created
in R.

The directory "leaks_on" contains the SLD, DLD, and TLD files for simulating
reactions with leaks enabled in Visual DSD, model checking and the outputed results
in PRISM, and BASH scripts for automating the entire process. The directory 
"leaks_off" contains the same but for reactions with leaks disabled. The directory 
"diagrams" contains figures generated from the results and an R script for analysing 
and producing the resulting data and figures, respectively.

For more information, see:

[PRISM 4.0: Verification of Probabilistic Real-Time Systems]
(https://link.springer.com/chapter/10.1007/978-3-642-22110-1_47)

[Leakless DNA Strand Displacement Systems]
(https://link.springer.com/chapter/10.1007/978-3-319-21999-8_9)

[Design and analysis of DNA strand displacement devices using probabilistic model checking]
(http://rsif.royalsocietypublishing.org/content/9/72/1470)

[Visual DSD: a design and analysis tool for DNA strand displacement systems]
(https://academic.oup.com/bioinformatics/article/27/22/3211/195595)
